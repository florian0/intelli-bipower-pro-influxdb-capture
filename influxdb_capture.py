import io
import serial
import struct
import hexdump
from influxdb import InfluxDBClient
from datetime import datetime
import pytz

buffer = b''

timezone = pytz.timezone('Europe/Berlin')

client = InfluxDBClient(host='localhost', port=8086, database="simprop")

with serial.Serial('/dev/ttyUSB0', 9600) as ser:
    while True:
        buffer += ser.read(64)
        
        if b'\r' in buffer:
            splits = buffer.split(b'\r')
            
            for split in splits[:-1]:
                if len(split) != 53:
                    continue
                
                if split[0] != 12:
                    print(f"Expected 12, got {split[0]}")
                
                binarydata = bytes.fromhex(split[1:].decode())
                
                currentVoltage, currentAmps, totalAmpsIn, cell1, cell2, cell3, cell4, cell5, cell6, unknown = struct.unpack('>xxxxxxHHHHHHHHHH', binarydata)
                
                print(currentVoltage, currentAmps, totalAmpsIn)
                print("Equalizer", cell1, cell2, cell3, cell4, cell5, cell6)
                print(unknown)
                
                jsondata = [{
                    "measurement": "intellibipower",
                    "time": datetime.now(timezone).isoformat(),
                    "fields": {
                        "voltage": currentVoltage / 1000,
                        "current" : currentAmps / 1000,
                        "charged": totalAmpsIn / 1000,
                        "cell1": cell1 / 1000,
                        "cell2": cell2 / 1000,
                        "cell3": cell3 / 1000,
                        "cell4": cell4 / 1000,
                        "cell5": cell5 / 1000,
                        "cell6": cell6 / 1000,
                    }
                }]
                    
                client.write_points(jsondata)
                
                
            buffer = splits[-1]
        
