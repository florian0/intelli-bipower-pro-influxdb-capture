# Simprop Intelli-BiPower PRO Data Logger

Link to Product: https://simprop.de/Seiten/Modellbau/0100846Intelli-BiPowerPRO.htm

I love this device. But the PC Software is a nightmare (sorry Simprop <3).

This project allows logging the data into InfluxDB and visualize it with Grafana.

![Battery charging](battery.png)

![Equalizer with three cells](equalizer-3-cells.png)


## Setup

1. Connect the charger to the PC using the PC interface adapter.
2. Figure out which tty it got assigned (usually something among `/dev/ttyUSBX`).
3. Put the correct device path in `influxdb_capture.py`.
4. Install pip requirements (`pip install -r requirements.txt`).
5. Run `docker-compose up -d` to start up the docker container.
6. Run the capture script (`python influxdb_capture.py`).

Grafana is available at http://localhost:3000

### Troubleshooting

If the script fails at opening the serial device, make sure that:

* Nothing is using the serial port already.
* Your user has permissions to use serial devices (group `dailout` is required on some systems).
* The serial device you're using the the right one.

## !!! Security !!!

NOTHING on this project is aimed towards being secure. There are NO PASSWORDS, no encryption, no anything. Do not run this on a public network and do not allow anyone to access your instance remotely UNLESS you are well aware of what you are doing.

I'm not responsible for the well-being of your computer.

## Hardware & Protocol

I've partially reverse engineered the protocol. The PC interface is a standard USB-to-Serial adapter. The settings are 9600 8N1. The protocol is single direction, meaning the device can only send data, but not receive any commands.

The data is updated in one second intervals. One data frame seems to be something like:

```
0xC | 52 bytes hexadecimal string | 0xD (\r)
0xC | 52 bytes hexadecimal string | 0xD (\r)
0xC | 52 bytes hexadecimal string | 0xD (\r)
```

After decoding the 52 bytes into binary data, you end up with 13 2-byte-words. For the Intelli-BiPower PRO, it seems to be always 13 words. Byte order is Big-Endian. The mapping of the 13 words is as following:

| Word | Meaning                        |
|------|--------------------------------|
|    0 |                                |
|    1 |                                |
|    2 |                                |
|    3 | Voltage (mV)                   |
|    4 | Current (mA)                   |
|    5 | Power put into Battery (mAh)   |
|    6 | Equalizer: Cell 1 (mV)         |
|    7 | Equalizer: Cell 2 (mV)         |
|    8 | Equalizer: Cell 3 (mV)         |
|    9 | Equalizer: Cell 4 (mV)         |
|   10 | Equalizer: Cell 5 (mV)         |
|   11 | Equalizer: Cell 6 (mV)         |
|   12 | Checksum ?                     |

I didn't figure out the first three words as they are always the same value. The first one seems like its are some kind of identifier for the procotol, maybe? Word 12 seems to be some kind of a checksum.

